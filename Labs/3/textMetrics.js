async function createMetrics(text) {
    if((!text) || typeof text !== 'string') throw 'Invalid paramter: text';
    text = text.toLowerCase();
    var ret = {};
    const ch = /[a-z]/g;
    const nh = /[^a-z]/g;
    ret.totalLetters = text.match(ch).length;
    ret.totalNotLetters = text.match(nh).length;
    const words = text.split(nh).filter(e => !(nh.test(e) || e === ''));
    ret.totalWords = words.length;
    ret.totalVowels = text.match(/[aeiou]/g).length;
    ret.totalConsonants = words.join('').match(/[^aeiou]/g).length;
    ret.uniqueWords = [...new Set(words)];
    ret.longWords = words.filter(word => word.length > 5).length;
    ret.averageWordLength = words.join('').length / words.length;
    ret.wordOccurrences = {};
    ret.uniqueWords.forEach(word => {
	let countExp = new RegExp(`${word}`, 'g');
	var count = 0;
	words.forEach(wrd => {
	    if(word === wrd)
		count++;
	});
	ret.wordOccurrences[word] = count;
    });
    return ret;
}

module.exports = {
    createMetrics: createMetrics
};
