const metrics = require('./textMetrics');
const fileData = require('./fileData');

const fs = require('fs');

const _start = async name => {
    if(!name || typeof name !== 'string') {
	throw "Invalid paramter";
    } else if(fs.existsSync(`${name}.results.json`)) {
	return console.log(await fileData.getFileAsJSON(`${name}.results.json`));
    } else {
	// no results file exists
	const contents = await fileData.getFileAsString(name);
	const analysis = await metrics.createMetrics(contents);
	await fileData.saveJSONToFile(`${name}.results.json`, analysis);
	return console.log(analysis);
    }
};

_start('chapter1.txt');
_start('chapter2.txt');
_start('chapter3.txt');
