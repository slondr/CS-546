const bluebird = require('bluebird');
const Promise = bluebird.Promise;
const fs = bluebird.promisifyAll(require('fs'));

async function getFileAsString(path) {
    if(!path) {
	throw 'You must provide a path';
    } else {
	return await fs.readFileAsync(path, 'utf8');
    }
}

const getFileAsJSON = async path => {
    if((!path) || (typeof path !== "string"))
       throw "Invalid parameter: Expected string";
    return JSON.parse(await getFileAsString(path));
};

async function saveStringToFile(path, text) {
    if(!(path && text))
	throw "Missing required parameter";
    await fs.writeFileAsync(path, text);
    return true;
}

const saveJSONToFile = async (path, obj) => {
    if((!path) || (typeof path !== "string")) {
	throw "Error in file path";
    } else if((!obj) || (typeof obj !== "object")) {
	throw "Error in object";
    } else {
	return await saveStringToFile(path, JSON.stringify(obj));
    }
};
    
module.exports = {
    getFileAsString: getFileAsString,
    getFileAsJSON: getFileAsJSON,
    saveStringToFile: saveStringToFile,
    saveJSONToFile: saveJSONToFile
};
