const animals = require('../collections').animals;

async function create(name, animalType) {
    // check function parameters
    if(!name || typeof name !== 'string')
	throw 'You must provide a name for the animal.';
    if(!animalType || typeof animalType !== 'string')
	throw 'You must provide a type of animal.';

    // Everything is good!
    const animalsCol = await animals();
    const insertRet = await animalsCol.insertOne({ name, animalType });
    if(insertRet.insertedCount === 0)
	throw 'Failed to add animal.';
    return await this.get(insertRet.insertedId);
}

const getAll = async () =>
      await (await animals()).find({}).toArray();
    


async function get(_id) {
    if(!_id)
	throw 'You must provide an id';
    const animal = await (await animals()).findOne({ _id });
    if(!animal)
	throw 'No animal with that id';
    return animal;
}

async function remove(_id) {
    if(!_id)
	throw 'You must provide an animal id to remove';
    const deleteRet = await (await animals()).deleteOne({ _id });
    if(deleteRet.deletedCount === 0)
	throw `Could not delete animal with an id of ${_id}`;
    return true;
}

async function rename(_id, newName) {
    if(!_id)
	throw 'You must provide an id';
    if(!newName || typeof newName !== 'string')
	throw 'You must provide a name for the animal!';

    // Parameters are good
    const animalsCol = await animals();
    const oldAnimal = get(_id);
    const newAnimal = { _id, name: newName, animalType: oldAnimal.animalType };
    const renameRet = await animalsCol.updateOne({ _id }, { $set: newAnimal });
    if(renameRet.modifiedCount === 0)
	throw 'Failed to update the animal';
    return await this.get(_id);
}

module.exports = { create, getAll, get, remove, rename };
