const progn = require('./data/animals');
const assert = require ('assert').strict;

async function testCreate() {
    console.log('Testing create');
    const dog = await progn.create('Fluffly', 'Dog');
    assert.equal(dog.name, 'Fluffly');
    assert.equal(dog.animalType, 'Dog');
    console.log('All create tests completed.');
}

async function testGetAll() {
    console.log('Testing getAll');
    const dog = await progn.create('Fluffly', 'Dog');
    const cat = await progn.create('Cheshire', 'Cat');
    const turtle = await progn.create('Ted', 'Turtle');
    assert(progn.getAll());
    console.log('All getAll tests completed');
}

async function testRemove() {
    console.log('Testing remove');
    const dog = await progn.create('Fluffly', 'Dog');
    assert(await progn.remove(dog._id));
    console.log('All remove tests completed');
}

async function testRename() {
    console.log('Testing rename');
    const dog = await progn.create('Fluffly', 'Dog');
    await progn.rename(dog._id, 'Fluffy');
    const newDog = await progn.get(dog._id);
    assert.equal(newDog.name, 'Fluffy');
    console.log('All rename tests completed');
}

/// Run the tests \\\

async function main() {
    console.log('||| Test Suite Beginning |||');
    return await Promise.all([
	testCreate(),
	testGetAll(),
	testRemove(),
	testRename()	  
    ]);  
}

(async () => await main())();
