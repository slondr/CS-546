const mongo = require('mongodb').MongoClient;
const settings = require('./settings');
const config = settings.config;

let _connection = undefined;
let _db = undefined;

module.exports = async () => {
    if(!_connection) {
	_connection = await mongo.connect(config.serverUrl, {
	    useNewUrlParser: true,
	    useUnifiedTopology: true
	});
	_db = await _connection.db(config.database);
    }
    return _db;
};
