const collections = require('../config/collections');
const albums = collections.albums;
const bands = require('./bands');

module.exports = {
    async getAlbum(_id) {
	if(!_id) throw 'Did not provide an id';
	const album = await (await albums()).findOne({ _id });
	if(!album) throw 'No album with that id';
	return album;
    },

    async getAllAlbums() {
	return await (await albums()).find({}).toArray();
    },

    async addAlbum(title, author, songs) {
	if(!title || typeof title !== 'string')
	    throw 'You must provide a title for the album!';
	if(!author || typeof author !== 'string')
	    throw 'You must provide an author of the album!';
	if(!songs || songs.length < 1)
	    throw 'Albums must contain at least one song!';

	// Inputs are good; insert new album into database
	albumCol = await albums();
	const insRet = await albumCol.insertOne({ title, author, songs });
	if(insRet.insertedCount === 0)
	    throw 'Failed to add album';

	// Album has been inserted; update relevant band
	const id = insRet.insertedId;
	const oldBand = bands.getBand(author);
	let bandAlbums = oldBand.albums;
	bandAlbums.push(id);
	bands.updateBand(author,
			 oldBand.bandName,
			 oldBand.bandMembers,
			 oldBand.yearFormed,
			 oldBand.genres,
			 oldBand.recordLabel,
			 bandAlbums
			);
	return await this.getAlbum(id);
    },

    async updateAlbum(_id, title, author, songs) {
	if(!title || typeof title !== 'string')
	    throw 'You must provide a title for the album!';
	if(!author || typeof author !== 'string')
	    throw 'You must provide an author of the album!';
	if(!songs || songs.length < 1)
	    throw 'Albums must contain at least one song!';
	
	// Inputs are good; insert new album into database
	const newAl = { title, author, songs };
	const upRet = await (await albums()).updateOne({ _id }, { $set: newAl });
	if(upRet.modifiedCount === 0) throw 'Failed to update album';
	return await this.getAlbum(_id);
    },

    async removeAlbum(_id) {
	if(!_id) throw 'You must provide an album ID to remove';
	const delRet = await (await albums()).deleteOne({ _id });
	if(delRet.deletedCount === 0) throw `Could not delete album with id ${_id}.`;
	return true;
    }
};
