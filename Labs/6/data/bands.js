const collections = require('../config/collections');
const bands = collections.bands;

module.exports = {
    async getBand(_id) {
	if(!_id) throw 'Did not provide an id';
	const bandCollection = await bands();
	const band = await bandCollection.findOne({ _id });
	if(!band) throw 'No band with that ID';
	return band;	
    },

    async getAllBands() {
	const bandCollection = await bands();
	return await bandCollection.find({}).toArray();
    },
    
    async addBand(bandName, bandMembers, yearFormed, genres, recordLabel) {
	if(!bandName || typeof bandName !== 'string')
	    throw 'You must provide a name for the band!';
	if(!bandMembers || !Array.isArray(bandMembers) || bandMembers.length < 1)
	    throw 'Bands must have at least one member!';
	if(!yearFormed || typeof yearFormed !== 'number')
	    throw 'Bands must have been formed in a valid year!';
	if(!genres || !Array.isArray(genres) || genres.length < 1)
	    throw 'Bands must have at least one valid genre!';
	if(!recordLabel || typeof recordLabel !== 'string')
	    throw 'Bands must be signed to a label!';
	

	// Everything is good
	const bandCollection = await bands();
	const newBand = { bandName, bandMembers, yearFormed, genres, recordLabel, albums: [] };
	const insertRet = await bandCollection.insertOne(newBand);
	if(insertRet.insertedCount === 0)
	    throw 'Failed to add band';
	const band = await this.getBand(insertRet.insertedId);
	return band;	    
    },

    async updateBand(bandId, bandName, bandMembers, yearFormed, genres, recordLabel, albums) {
	if(!bandId) throw 'You must provide an id';
	if(!bandName || typeof bandName !== 'string')
	    throw 'You must provide a name for the band!';
	if(!bandMembers || !Array.isArray(bandMembers) || bandMembers.length < 1)
	    throw 'Bands must have at least one member!';
	if(!yearFormed || typeof yearFormed !== 'number')
	    throw 'Bands must have been formed in a valid year!';
	if(!genres || !Array.isArray(genres) || genres.length < 1)
	    throw 'Bands must have at least one valid genre!';
	if(!recordLabel || typeof recordLabel !== 'string')
	    throw 'Bands must be signed to a label!';
	if(!albums)
	    albums = [];

	// Everything is good
	const bandCollection = await bands();
	const newBand = { bandName, bandMembers, yearFormed, genres, recordLabel, albums };
	const updatedRet = await bandCollection.updateOne({ _id: bandId }, { $set: newBand });
	if(updatedRet.modifiedCount === 0) throw 'Failed to update band';
	return await this.getBand(bandId);	
    },

    async removeBand(_id) {
	if(!_id) throw 'You must provide a band ID to remove';
	const bandCol = await bands();
	const deleteRet = await bandCol.deleteOne({ _id });
	if(deleteRet.deletedCount === 0) throw `Could not delete band with id of ${_id}`;
	return true;
    }
};
