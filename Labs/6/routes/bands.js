const express = require('express');
const router = express.Router();
const data = require('../data');

router.use(express.json());

router.get('/:id', async (req, res) => {
    try {
	const band = await data.bands.getBand(req.params.id);
	band.albums = band.albums.map(id => data.albums.getAlbum(id));
	res.json(band);	      
    } catch(e) {
	res.status(404).json({ message: 'band not found '});
    }
});

router.get('/', async (req, res) => {
    try {
	const bandList = await data.bands.getAllBands();
	bandList.forEach(band => band.albums = band.albums.map(id => data.albums.getAlbum(id)));
	res.json(bandList);
    } catch(e) {
	res.status(500).send();
    }
});

router.post('/', async (req, res) => {
    if(('bandName' in req.body)
       && ('bandMembers' in req.body)
       && ('yearFormed' in req.body)
       && ('genres' in req.body)
       && ('recordLabel' in req.body)) {
	// schema was good
	res.status(200).json(
	    await data.bands.addBand(
		req.body.bandName,
		req.body.bandMembers,
		req.body.yearFormed,
		req.body.genres,
		req.body.recordLabel
	    )
	);
    } else {
	res.status(400).send('Bad schema in request');
    }
});

router.put('/:id', async (req, res) => {
    if(('bandName' in req.body)
       && ('bandMembers' in req.body)
       && ('yearFormed' in req.body)
       && ('genres' in req.body)
       && ('recordLabel' in req.body)) {

	// schema was good

	let oldAlbums = undefined;

	try {
	    oldAlbums = data.bands.getBand(req.params.id).albums;
	} catch(e) {
	    res.status(404).send('Band not found');
	}
	
	try {
	    const newBand = await data.bands.updateBand(
		req.params.id,
		req.body.bandName,
		req.body.bandMembers,
		req.body.yearFormed,
		req.body.genres,
		req.body.recordLabel,
		oldAlbums
	    );
	    
	    res.status(200).json(newBand);
	} catch(e) {
	    res.status(400).send(e);
	}
    } else {
	res.status(400).send('Bad schema');
    }
});

router.delete('/:id', async (req, res) => {
    if(!req.params.id) res.status(400).send('ID not provided');
    try {
	await data.bands.getBand(req.params.id);
    } catch(e) {
	res.status(404).send(e);
    }
    // band exists in the db
    try {
	await data.bands.removeBand(req.params.id);
	res.status(200).send('ok');
    } catch(e) {
	res.status(500).send(e);
    }
});

module.exports = router;
