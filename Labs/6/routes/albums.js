const express = require('express');
const router = express.Router();
const data = require('../data');

function addAuthorToAlbum(album) {
    const oldAuthor = album.author;
    album.author = {
	_id: oldAuthor,
	bandName: data.bands.getBand(oldAuthor).bandName
    };
    return album;
}

router.use(express.json());

router.get('/', async (req, res) => {
    try {
	let albums = await data.albums.getAllAlbums();
	const albumsWithAuthor = albums.map(album => addAuthorToAlbum(album));
	return albumsWithAuthor;
    } catch(e) {
	return res.status(500).send();
    }
});

router.get('/:id', async (req, res) => {
    try {
	let album = addAuthorToAlbum(await data.albums.getAlbum(req.params.id));
	return res.status(200).json(album);
    } catch(e) {
	return res.status(500).send('error');
    }
});

router.post('/', async (req, res) => {
    if(('title' in req.body)
       && ('author' in req.body)
       && ('songs' in req.body)) {
	const ret = await data.albums.addAlbum(
		req.body.title,
		req.body.author,
		req.body.songs
	);
	return res.status(200).json(
	    addAuthorToAlbum(data.albums.getAlbum(ret.insertedId))
	);
    } else {
	return res.status(400).send('Bad schema in request: Missing key');
    }
});

router.patch('/:id', async (req, res) => {
    if(!(('newTitle' in req.body) || ('newSongs' in req.body))) {
	return res.status(400).send('No patchable content found');
    } else {
	// we received something patchable
	let ret = undefined;
	try {
	    ret = await data.albums.getAlbum(req.params.id);
	} catch(e) {
	    res.status(404).send(e);
	    return false;
	}
	if('newTitle' in req.body) {
	    ret.title = req.body.newTitle;
	}
	if('newSongs' in req.body) {
	    ret.songs = ret.songs.push(req.body.newSongs);
	}
	ret = addAuthorToAlbum(ret);
	await data.albums.updateAlbum(req.params.id, ret.title, ret.author, ret.songs);
	res.status(200).send(ret);
	return ret;
    }
});

router.delete('/:id', async (req, res) => {
    try {
	let record = data.albums.getAlbum(req.params.id);
	await data.albums.removeAlbum(req.params.id);
	record.deleted = true;
	return res.status(200).json(addAuthorToAlbum(record));
    } catch(e) {
	return res.status(400).send(e);
    }
});

module.exports = router;
