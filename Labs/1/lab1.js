const questionOne = function questionOne(arr) {
    return arr.map(a => a * a).reduce((a, b) => a + b);
};

const questionTwo = function questionTwo(num) { 
    var trail1 = 0;
    var trail0 = 0;
    var cur = 1;
    for(let i = 1; i < num; ++i) {
	trail1 = trail0;
	trail0 = cur;
	cur = trail1 + trail0;
    }
    return cur;
};

const questionThree = function questionThree(text) {
    return text.split("").filter(a => a === 'a'
				 || a === 'e'
				 || a === 'i'
				 || a === 'o'
				 || a === 'u').length;
};

const questionFour = function questionFour(num) {
    if(num < 0) return NaN;
    let ret = 1;
    for(let i = 1; i <= num; i++) {
	ret *= i;
    }
    return ret;
};

module.exports = {
    firstName: "YOUR FIRST NAME", 
    lastName: "YOUR LAST NAME", 
    studentId: "YOUR STUDENT ID",
    questionOne,
    questionTwo,
    questionThree,
    questionFour
};
