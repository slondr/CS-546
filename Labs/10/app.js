const express = require('express');
const app = express();
const session = require('express-session');
const handlebars = require('express-handlebars');
const routes = require('./routes');
const stat = express.static(__dirname + '/public');

app.use('/public', stat);

app.use(express.json());

app.use(express.urlencoded({ extended: true }));

app.use(session({
    name: 'AuthCookie',
    secret: 'some secret string!',
    saveUninitialized: true,
    resave: false
}));

app.use(async (request, result, next) => {
    console.log(`REQUEST
${request.method} ${request.originalUrl}
User is ${!request.session.user? 'not ' : ''}authenticated.
[${new Date().toUTCString()}]
`);
    next();
});

app.use('/private', (request, result, next) => request.session.user?
	next() : result.redirect('/fail'));

app.use('/login', (request, result, next) => {
    if(!request.session.user) {
	request.method = 'POST';
	return next();
    } else {
	return result.redirect('/private');
    }
});


app.engine('handlebars', handlebars({ defaultLayout: 'main' }));
app.set('view engine', 'handlebars');

// initialize express server
routes(app);
app.listen(3000, () => {
    console.log('Server running at http://localhost:3000');
    require('child_process').exec('xdg-open "http://localhost:3000"');
});
