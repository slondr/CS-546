const router = require('express').Router();
const bcrypt = require('bcrypt');
const data = require('../data');

router.get('/', async (rq, rs) => {
    if(rq.session.user) {
	rs.redirect('/private');
    } else {
	rs.render('pages/index', { title: 'Log In' });
    }
});

router.post('/login', async (rq, rs) => {
    const username = rq.body.username ||  '';
    const password = rq.body.password || '';
    
    // compare the password to the hashed password from the users file
    for(const user of data) {
	if((username === user.username) && await bcrypt.compare(password, user.hashedPassword)) {
	    rq.session.user = user._id;
	    console.log(`matched ${user.username}`);
	    break;
	} else {
	    console.log(`${username}/${password} did not match ${user.username}`);
	}
    }

    rs.redirect('/private');
});

router.get('/logout', async (rq, rs) => {
    rq.session.destroy();
    rs.render('pages/logout', { title: 'Log Out' });
});

router.get('/fail', async (rq, rs) => rs.status(401).render('pages/failure', {
    title: 'Invalid Credentials'
}));

module.exports = router;
