const express = require('express');
const router = express.Router();
const data = require('../data');

router.get('/', async (rq, rs) => {
    for(const user of data) {
	if(user._id === rq.session.user) {
	    rs.render('pages/private', { user, title: `Welcome, ${user.firstname} ${user.lastname}.` });
	    break;
	}
    }
});

module.exports = router;
