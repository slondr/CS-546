// There are four routes for this project: /, /login, /private, and /logout
// / redirects to /private if the user is authenticated
// If the user is not logged in/authenticated, they will see a login screen

const private = require('./private');
const users = require ('./users');

const constructorMethod = app => {
    app.use('/', users);
    app.use('/private', private);
    app.use('*', (rq, rs) => rs.sendStatus(404));
};

module.exports = constructorMethod;
