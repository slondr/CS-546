const router = require('express').Router();
const bcrypt = require('bcrypt');

router.get('/', async (rq, rs) => rs.render('pages/index', {
    title: 'Log In'
}));

router.post('/', async (rq, rs) => rs.json({ route: '/users', method: rq.method }));
router.post('/login', async (rq, rs) => {
    rq.session.destroy();
    rs.send('logged out');
});

module.exports = router;
