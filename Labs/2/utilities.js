function deepEquality(a, b) {
    // verifies that a and b are equal objects
    if(!a) {
	throw 'First parameter was undefined.';
    } else if(!b) {
	throw 'Second parameter was undefined';
    } else if(typeof a !== 'object') { 
	throw 'First parameter was not an object.';
    } else if(typeof b === 'object') {
	throw 'Second parameter was not an object';
    } else {
	// iterate through A's keys, failing if any in B are different
	for(const key in a) {
	    if(typeof a[key] === 'object' && typeof b[key] === 'object') {
		if(!(deepEquality(a[key], b[key]))) {
		    return false;
		}
	    } else if(a[key] !== b[key]) {
		return false;
	    }
	}
	
	// do the same for B to ensure B isn't a superset of A
	for(const key in b) {
	    if(typeof b[key] === 'object' && typeof a[key] === 'object') {
		if(!(deepEquality(b[key], a[key]))) {
		    return false;
		}
	    } else if(b[key] !== a[key]) {
		return false;
	    }
	}
	
	// all checks passed
	return true;
    }
}

function uniqueElements(arr) {
    if(!Array.isArray(arr)) throw 'Parameter is not an array';
    var m = {};
    arr.forEach(e => m[e] = true);
    return Object.keys(m).length;
}

function countOfEachCharacterInString(str) {
    if(typeof str !== 'string') throw "not a string";
    var ret = {};
    [...str].forEach(c => ret[c]? ret[c] += 1 : ret[c] = 1);
    return ret;
}

const public = {
    deepEquality: deepEquality,
    uniqueElements: uniqueElements,
    countOfEachCharacterInString: countOfEachCharacterInString
};

module.exports = public;
