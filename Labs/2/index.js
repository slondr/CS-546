const geometry = require('./geometry');
const util = require('./utilities');

var testIndex = 0;

function test(predicate) {
    if(predicate) {
	console.log(`Test ${testIndex++} successful.`);
    } else {
	console.error(`Test ${testIndex++} failed.`);
    }
}

const approx = (discrete, rough) => Math.abs(discrete - rough) < 1;

test(true);

const vr = geometry.volumeOfRectangularPrism;
test(vr(1, 2, 3) === 6);
test(vr(4, 7, 9) === 252);
test(vr(1, 1, 4) === 4);
test(vr(7, 9, 400) === 25200);
test(vr(1, 1, 1) === 1);

const sr = geometry.surfaceAreaOfRectangularPrism;
test(sr(1, 1, 1) === 6);
test(sr(2, 2, 2) === 24);
test(sr(0.5, 0.5, 0.5) === 1.5);
test(sr(4, 8, 1) === 88);
test(sr(100, 800, 145) === 421000);

const vs = geometry.volumeOfSphere;
test(approx(vs(1), 4.18879));
test(approx(vs(2), 33.5103));
test(approx(vs(33), 150533));
test(approx(vs(191), 29186950));
test(approx(vs(25.96), 73282.9));

const ss = geometry.surfaceAreaOfSphere;
test(approx(vs(1), 4.18));
test(approx(vs(2), 33.51));
test(approx(vs(57), 775734.62));
test(approx(vs(7.76), 1957.37));
test(approx(vs(8000.1), 2144741010627.9));

test(util.deepEquality({a: 2, b: 3}, {a: 2, b: 3}));
test(util.deepEquality({a: 2, b: 3}, {a: 2, b: 4})); // should fail
test(util.deepEquality({a: 2, b: 3}, {a: 2, b: 3, c: 5})); // should fail
test(util.deepEquality({a: 2}, {a: 2, b: 3})); // should fail
test(util.deepEquality({a: {b: 3, c: 6}}, {a: {b: 3, c: 6}}));
test(util.deepEquality({a: {b: 3, c: 6}}, {a: {b: 3, c: 7}})); // should fail

test(util.uniqueElements([]) === 0);
test(util.uniqueElements(['a']) === 1);
test(util.uniqueElements(['a', 'a', 'a', 'a', 'a', 'a', 'b']) === 2);
test(util.uniqueElements(['a', 'b', 'c', 'd', 'e', 'e', 'd']) === 5);
test(util.uniqueElements([1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]) === 12);

test(util.deepEquality(util.countOfEachCharacterInString('e'), {e: 1}));
test(util.countOfEachCharacterInString('a')['a'] === 1);
test(util.countOfEachCharacterInString('hello world')['l'] === 3);
test(util.countOfEachCharacterInString('foobar').o === 2);
test(util.countOfEachCharacterInString('wew lad')[' '] === 1);
