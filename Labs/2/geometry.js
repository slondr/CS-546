module.exports = {
    volumeOfRectangularPrism: (l, w, h) => {
	if(l <= 0 || w <= 0 || h <= 0) {
	    throw "Function parameter out of bounds";
	} else {
	    return l * w * h;
	}
    },
    surfaceAreaOfRectangularPrism: (l, w, h) => {
	if(l <= 0 || w <= 0 || h <= 0) {
	    throw "Function parameter out of bounds";
	} else {
	    return 2 * l * w + 2 * l * h + 2 * w * h;
	}
    },
    volumeOfSphere: r => {
	if(r <= 0) {
	    throw "Function parameter out of bounds";
	} else {
	    return (4/3) * Math.PI * Math.pow(r, 3);
	}
    },
    surfaceAreaOfSphere: r => {
	if(r <= 0) {
	    throw "Function parameter out of bounds";
	} else {
	    return 4 * Math.PI * Math.pow(r, 2);
	}
    }
};
