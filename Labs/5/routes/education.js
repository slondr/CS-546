const router = require('express').Router();

router.get('/', async (req, res) => {
    try {
	res.json([{
	    schoolName: 'Stevens Institute of Technology',
	    degree: 'Computer Science',
	    favoriteClass: 'CS 546',
	    favoriteMemory: 'Getting to class on time once'
	}, {
	    schoolName: 'Rochester Institute of Technology',
	    degree: 'Computer Science',
	    favoriteClass: 'SIS 340',
	    favoriteMemory: 'The food'
	}, {
	    schoolName: 'Ocean County Vocational Technical School',
	    degree: 'Computer Science',
	    favoriteClass: 'Computer Science',
	    favoriteMemory: 'Picnic'
	}]);
    } catch(e) {
	res.status(500).send();
    }
});

module.exports = router;
