const router = require('express').Router();

router.get('/', async (req, res) => {
    try {
	res.json({
	    storyTitle: "The Title of the Story",
	    story: "She's quiet, impatient and sarcastic. This is to be expected from somebody with her horrifying past.\nShe was born and grew up in a poor family in a major capital, she lived without worry until she was about 9 years old, but at that point life took a turn for the worst.\nShe lost her mother in a rebellion and was now alone, miserable and abandoned. While persued by strangers she had to survive in a pitiless world. But with her determination and strength, she managed to go beyond expectations and escape hell. This has turned her into the woman she is today.\nSettled down and with some peace and quiet, she now works as a travelling trader. By doing so, she hopes to shed the memories of the past and finally find joys and comforts of life she has never had."
	});
    } catch(e) {
	res.status(500).send();
    }
});

module.exports = router;
