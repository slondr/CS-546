const router = require('express').Router();

router.get('/', async (req, res) => {
    try {
	res.json({
	    name: "Eric S. Londres",
	    cwid: "10822608",
	    biography: "He was born in a successful family in an important port. He lived out of trouble until he was about 16 years old, but at that point life began to change.\nHe started working for the family company and was meeting a lot of influential people. Alongside great friends, he succeeded in a fast changing world. But with his wisdom and capability, there's nothing to stop him from reaching full potential. He could quickly become somebody who could change the world.",
	    favoriteShows: ['Daredevil', 'Jessica Jones', 'Luke Cage'],
	    hobbies: ['Reading', 'Donating Blood']
	});
    } catch(e) {
	res.status(500).send();
    }
});

module.exports = router;
