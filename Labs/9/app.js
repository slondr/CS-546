const express = require('express');
const app = express();
const stat = express.static(`${__dirname}/public`);

app.use('/', stat);		// Serve files from '/public' on '/'

app.listen(3000, () => console.log('Server running on port 3000.'));
