const express = require('express');
const router = express.Router();

function isPalindrome(text)  {
    if(!text) return false;
    const string = sanitize(text);
    console.log(string);
    if(string.length === 1) return true; // single-character strings are palindromes
    else if(string.length < 1) return false; // invalid input
    for(let i = 0; i < string.length / 2; ++i) {
	if(string[i] !== string[string.length - i - 1])
	    return false; // there is a mismatch; no palindrome
    }
    return true; // there were no mismatches; palindrome
}

const sanitize = text => {
    console.log(`Received ${text}`);
    let ret = text.toLowerCase().replace(/\W+/g, '');
    console.log(`Returning ${ret}`);
    return ret;
};

router.get('/', async (req, res) => {
    res.render('pages/index', {
	title: 'The Best Palindrome Checker in the World!'
    });
});

router.post('/result', async (req, res) => {
    let text = req.body['text-to-test'];
    let errors = [];

    
    try {
	if(!text)
	    errors.push('No text provided');
	
	if(sanitize(text).length < 1)
	    errors.push('No alphanumeric characters!');
    } catch(e) {
	errors.push(e);
    }
    
    if(errors.length > 0) {
	res.status(400).render('pages/result', {
	    errors,
	    title: 'The Palindrome Results!',
	    hasErrors: true,
	    success: false,
	    'text-to-test': text
	});
	return;
    }

    res.render('pages/result', {
	hasErrors: false,
	title: 'The Palindrome Results!',
	success: isPalindrome(text),
	'text-to-test': text
    });
});

module.exports = router;
