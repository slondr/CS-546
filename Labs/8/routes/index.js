const formRoutes = require('./form');

const constructorMethod = app => {
    app.use('/', formRoutes);
    app.use('/result', formRoutes);
};

module.exports = constructorMethod;
