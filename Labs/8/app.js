const express = require('express');
const handlebars = require('express-handlebars');
const app = express();
const stat = express.static(__dirname + '/public');
const routes = require('./routes');

app.use('/public', stat);
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.engine('handlebars', handlebars({ defaultLayout: 'main' }));
app.set('view engine', 'handlebars');

routes(app);

app.listen(3000, () => {
    console.log("We have a server!\nYour routes will be running on port 3000.");
});

