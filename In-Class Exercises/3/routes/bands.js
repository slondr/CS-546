const router = require('express').Router();
const data = require('../data');

router.get('/:id', async (req, res) => {
    try {
	const band = await data.bands.getBand(req.params.id);
	res.json(band);	      
    } catch(e) {
	res.status(404).json({ message: 'band not found '});
    }
});

router.get('/', async (req, res) => {
    try {
	const bandList = await data.bands.getAllBands();
	res.json(bandList);
    } catch(e) {
	res.status(500).send();
    }
});

module.exports = router;
