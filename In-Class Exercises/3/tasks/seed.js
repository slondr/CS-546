const dbConnection = require('../config/connection');
const data = require ('../data/');
const bands = data.bands;

async function main() {
    const db = await dbConnection();
    await db.dropDatabase();
    bands.addBand('Muse',
		  ['Matthew Bellamy', 'Christopher Tony Wolstenholme', 'Dominic Howard'],
		  1994,
		  ['Post-Grunge', 'Alternative Rock', 'Space Rock', 'Progressive Rock', 'Hard Rock', 'Art Rock'],
		  'Warner Records Inc.');
    bands.addBand('The White Stripes', ['Jack White', 'Meg White'], 1997, ['Garage Rock', 'Blues'], 'Third Man Records');
    bands.addBand('Soundgarden', ['Chris Cornell', 'Kim Thayil', 'Ben Shepherd', 'Matt Cameron'], 1984, ['Grunge'], 'Republic');
    bands.addBand('Alice in Chains', ['Jerry Cantrell', 'Layne Staley', 'Mike Starr', 'Sean Kinney'], 1987, ['Grunge'], 'Columbia');
    bands.addBand('Mother Love Bone', ['Jeff Ament', 'Bruce Fairweather', 'Andrew Wood', 'Greg Gilmore', 'Stone Gossard'],
		  1987, ['Alternative Rock', 'Proto-Grunge', 'Glam Metal'], 'Mercury');
}

main().catch(console.log);
