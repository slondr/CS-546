const d = require('./dictionary');

try {
    console.log(d.lookupDefinition('programming'));
} catch(e) {
    console.error(e);
}

try {
    console.log(d.lookupDefinition('charisma'));
} catch(e) {
    console.error(e);
}

try {
    console.log(d.lookupDefinition('foobar'));
} catch(e) {
    console.error(e);
}

try {
    console.log(d.getWord('to make an official decision about who is right in (a dispute) : to settle judicially'));
} catch(e) {
    console.error(e);
}

try {
    console.log(d.getWord('To act as a detective : search for information'));
} catch(e) {
    console.error(e);
}

try {
    console.log(d.getWord('foobar'));
} catch(e) {
    console.error(e);
}
