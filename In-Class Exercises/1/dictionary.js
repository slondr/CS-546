const words = {
    programming: "The action or process of writing computer programs.",
    charisma: "A personal magic of leadership arousing special popular loyalty or enthusiasm for a public figure (such as a political leader)",
    sleuth: "To act as a detective : search for information",
    foray: "A sudden or irregular invasion or attack for war or spoils : raid",
    adjudicate: "to make an official decision about who is right in (a dispute) : to settle judicially"
};

const checkInput = s => {
    if(typeof s === "string") {
	return s;
    } else {
	throw "Not a string";
    }
};

const lookupDefinition = s => {
    const definition = words[checkInput(s)];
    if(definition) return definition;
    throw "not found";
};

const getWord = s => {
    const definition = checkInput(s);
    var word = Object.keys(words).find(assoc => words[assoc] === definition);
    if(word) {
	return word;
    } else {
	throw "definition not found";
    }
};

exports.lookupDefinition = lookupDefinition;
exports.getWord = getWord;
