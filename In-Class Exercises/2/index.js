const animals = require('./data/animals');

async function main() {
    // [1]
    const sasha = await animals.create('Sasha', 'Dog');

    // [2]
    console.log(sasha);

    // [3]
    const lucy = await animals.create('Lucy', 'Dog');

    // [4]
    const step4 = await animals.getAll();
    step4.forEach(animal => console.log(animal));

    // [5]
    const duke = await animals.create('Duke', 'Walrus');

    // [6]
    console.log(duke);

    // [7]
    const sashita = await animals.rename(sasha._id, 'Sashita');

    // [8]
    console.log(sashita);

    // [9]
    await animals.remove(lucy._id);

    // 10
    const step10 = await animals.getAll();
    step10.forEach(animal => console.log(animal));

}
