const progn = require('./bands');

async function main() {
    // (1) create a band with all the fields
    const muse = await progn.addBand('Muse',
				     ['Matthew Bellamy', 'Christopher Tony Wolstenholme', 'Dominic Howard'],
				     1994,
				     ['Post-Grunge', 'Alternative Rock', 'Space Rock', 'Progressive Rock', 'Hard Rock', 'Art Rock'],
				     'Warner Records Inc.');
    // (2) Log the band...
    console.log(muse);
    // ... and then create a new band.
    const whiteStripes = await progn.addBand('The White Stripes', ['Jack White', 'Meg White'], 1997, ['Garage Rock', 'Blues'], 'Third Man Records');
    
    // (3) After the 2nd band is inserted, query all bands and log them
    console.log(await progn.getAllBands());

    // (4) After all the bands are logged, remove the first band
    await progn.removeBand(muse._id);

    // (5) Query all the remaining bands and log them
    console.log(await progn.getAllBands());

    // (6) Update the remaining band
    const raconteurs = await progn.updateBand(whiteStripes._id, 'The Raconteurs', ['Jack White', 'Brendan Benson', 'Patrick Keeler', 'Jack Lawrence'], 2005, ['Garage Rock', 'Indie Rock', 'Alternative Rock'], 'Third Man Records');

    // (7) Log the band that has been updated
    console.log(raconteurs);
}

main();
