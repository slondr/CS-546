const progn = require('./bands');
const assert = require('assert').strict;




async function testAddBand() {
    // also tests getBand()
    console.log('Testing addBand');
    const whiteStripes = await progn.addBand('The White Stripes', ['Jack White', 'Meg White'], 1997, ['Garage Rock', 'Blues'], 'Third Man Records');
    assert.equal(whiteStripes.bandName, 'The White Stripes');
    assert.deepEqual(whiteStripes.genres, ['Garage Rock', 'Blues']);

    const muse = await progn.addBand('Muse',
				     ['Matthew Bellamy', 'Christopher Tony Wolstenholme', 'Dominic Howard'],
				     1994,
				     ['Post-Grunge', 'Alternative Rock', 'Space Rock', 'Progressive Rock', 'Hard Rock', 'Art Rock'],
				     'Warner Records Inc.');
    assert.equal(muse.bandName, 'Muse');
    assert.deepEqual(muse.bandMembers, ['Matthew Bellamy', 'Christopher Tony Wolstenholme', 'Dominic Howard']);
    assert.equal(muse.yearFormed, 1994);
    assert.deepEqual(muse.genres, ['Post-Grunge', 'Alternative Rock', 'Space Rock', 'Progressive Rock', 'Hard Rock', 'Art Rock']);
    assert.equal(muse.recordLabel, 'Warner Records Inc.');
    
    console.log('All addBand tests completed.');
}

async function testGetAllBands() {
    console.log('Testing getAllBands');
    await progn.addBand('The White Stripes', ['Jack White', 'Meg White'], 1997, ['Garage Rock', 'Blues'], 'Third Man Records');
    await progn.addBand('Muse', ['Matthew Bellamy', 'Christopher Tony Wolstenholme', 'Dominic Howard'], 1994,
			['Post-Grunge', 'Alternative Rock', 'Space Rock', 'Progressive Rock', 'Hard Rock', 'Art Rock'],
			'Warner Records Inc.');
    assert(progn.getAllBands()); // just checks that we got something, doesn't check for correctness
    console.log('All getAllBands tests completed.');
}

async function testUpdateBand() {
    console.log('Testing updateBands');
    const oldBand = await progn.addBand('The White Stripes', ['Jack White', 'Meg White'], 1997, ['Garage Rock', 'Blues'], 'Third Man Records');
    await progn.updateBand(oldBand._id, oldBand.bandName, oldBand.bandMembers, 2007, oldBand.genres, oldBand.recordLabel);
    const newBand = await progn.getBand(oldBand._id);
    assert.equal(newBand.yearFormed, 2007);
    console.log('All updateBand tests completed');
    
}

async function testRemoveBand() {
    console.log('Testing removeBand');
    const band = await progn.addBand('The White Stripes', ['Jack White', 'Meg White'], 1997, ['Garage Rock', 'Blues'], 'Third Man Records');
    assert(await progn.removeBand(band._id));
    console.log('All removeBand tests completed');
}

async function main() {
    console.log('||| Beginning Test Suite |||');
    return await Promise.all([
	testAddBand(),
	testGetAllBands(),
	testUpdateBand(),
	testRemoveBand()
    ]);
}

(async () => await main())();
